package Calculator11;

import java.util.ArrayList;

public class Result {

	public static String main(String userInput) {

		// Seperating numbers from operations
		ArrayList<String> numbers = NumberSeperators.numberSeperator(userInput);
		ArrayList marks = new ArrayList(NumberSeperators.markSeperator(userInput));

		// Checks if everything with the equation matches.
		String check = FailureChecks.main(numbers, marks);
		/*
		 * If there was an error, then the error will be deposited in check
		 * String and the if statement checks if it's empty. If it isn't empty
		 * it will return the string, if it's empty then it means that no error
		 * was found and it will continue
		 */
		if (!check.isEmpty())
			return check;

		// Creates a double value from strings.
		ArrayList<Double> doubleNumbers = new ArrayList<Double>(NumberSeperators.DoubleCreator(marks, numbers));

		// If the marks contains only 1 parentheses then the calculation will be
		// done at Calculator class.
		if ((marks.contains("(") && marks.contains(")")) && marks.indexOf("(") == marks.lastIndexOf("(")
				&& marks.indexOf(")") == marks.lastIndexOf(")")) {
			return Calculator.ParenthesesCalculator(doubleNumbers, marks).toString();
		}
		// If the equation doesn't contain any marks then it is sent directly to
		// calculator main class.
		if (!marks.contains("(") && !marks.contains(")")) {
			return Calculator.main(doubleNumbers, marks).toString();
		}

		/*
		 * If the calculation contains several parentheses then it will be sent
		 * to be reassembled and after that it will be send to be calculated.
		 */
		ArrayList<String> reassembled = new ArrayList<String>(Reassembling.reassemble(doubleNumbers, marks));
		return ReassembledCalculator.DividingUp(reassembled).toString();

	}

}
