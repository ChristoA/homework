package Calculator11;

import java.util.ArrayList;

public class NumberSeperators {

	// This method separates numbers from any other marks.
	public static ArrayList<String> numberSeperator(String input) {

		// StringBuilder is created to be able to check character at x spot.
		StringBuilder sbNumbers = new StringBuilder();

		// ArrayList is created to be able to put every number into a single
		// list
		ArrayList<String> arrayNumbers = new ArrayList<String>();

		/*
		 * A loop to check if Character at x location is a digit or not, if it's
		 * a digit then it is added to StringBuilder.
		 */
		for (int i = 0; i < input.length(); i++) {
			/*
			 * J is made to check if the value is a number or not. If it is a
			 * number then it puts it into the sbNumbers and puts I value to j+1
			 * so it would not add same value several times If it's not a value
			 * then it breaks, and goes to if statement.
			 */
			for (int j = i; j < input.length(); j++) {
				if (Character.isDigit(input.charAt(j))) {
					sbNumbers.append(input.charAt(j));
					i = j + 1;
				} else {
					break;
				}
			}
			/*
			 * If the sbNumbers contains anything then it is added to
			 * arrayNumbers, the check is done so it wouldn't add empty space.
			 * After the value is added StringBuilder is cleared and the cycle
			 * repeats.
			 */
			if (sbNumbers.length() > 0) {
				arrayNumbers.add(sbNumbers.toString());
				sbNumbers.delete(0, sbNumbers.length());
			}
		}
		/*
		 * Returns the arrayNumbers which contains every number in the equation.
		 */
		return arrayNumbers;
	}

	// Seperates marks from numbers
	public static ArrayList markSeperator(String input) {

		// StringBuilder is created to be able to check character at x spot.
		StringBuilder sbMarks = new StringBuilder();
		// ArrayList is created to be able to put every mark into a single list
		ArrayList arrayMarks = new ArrayList();

		/*
		 * A loop to check if Character at x location is a digit or not, if it's
		 * not a digit then it is added to StringBuilder.
		 */
		for (int i = 0; i < input.length(); i++) {
			/*
			 * J is made to check if the value is a number or not. If it's not a
			 * number then it puts it into the sbMarks and puts I value to j+1
			 * so it would not add same mark several times If it's a value then
			 * it breaks, and goes to if statement.
			 */
			for (int j = i; j < input.length(); j++) {
				if (!Character.isDigit(input.charAt(j))) {
					sbMarks.append(input.charAt(j));
					i = j + 1;
					/*
					 * If the sbMarks contains anything then it is added to
					 * arrayMarks, the check is done so it wouldn't add empty
					 * space. After the mark is added StringBuilder is cleared
					 * and the cycle repeats.
					 */
					if (sbMarks.length() > 0) {
						arrayMarks.add(sbMarks.toString());
						sbMarks.delete(0, sbMarks.length());
					} else {
						break;
					}
				}

			}
		}
		/*
		 * Returns the arrayMarks which contains every mark in the equation.
		 */
		return arrayMarks;
	}

	public static ArrayList<Double> DoubleCreator(ArrayList marks, ArrayList<String> numbers) {

		// doubleArray is created to store the new double values.
		ArrayList<Double> doubleArray = new ArrayList<Double>();
		// tempMarks is created to hold temporary marks (will not be used later)
		ArrayList<String> tempMarks = new ArrayList<String>(marks);

		// make comas into dot (in case user uses comas)
		for (int i = 0; i < tempMarks.size(); i++) {
			if (tempMarks.get(i).equals(",")) {
				tempMarks.remove(i);
				tempMarks.add(i, ".");
			}
		}

		// removes all marks so indexes would be correct
		while (tempMarks.contains("(") && tempMarks.contains(")")) {
			tempMarks.remove("(");
			tempMarks.remove(")");
		}

		// removes all dots from original marks ArrayList.
		while (marks.contains(".") || marks.contains(",")) {
			marks.remove(".");
			marks.remove(",");
		}

		/*
		 * int j is for marks. If at spot j is a mark a dot, then it makes the
		 * array values into a double. If this happens then mark at j is removed
		 * and j value is decreased by 1 so it wouldn't go over the next mark.
		 * After the if statement ends 1 is added to j, this is so when there
		 * isn't a dot, then it proceeds to next mark.
		 */
		int j = 0;

		/*
		 * Result is created to add two integers together to create a double for
		 * example integer 2 and integer 4 added together will make a double 2.4
		 */
		String result;

		for (int i = 0; j < tempMarks.size(); i++) {
			/*
			 * Checks if the mark is a dot or not, if it is then it adds the
			 * number corresponding to the mark and the number after it to
			 * create a string of them. Then the dot is removed from temporary
			 * marks and the two numbers are removed from numbers array. After
			 * that the number gotten is added to numbers array and the loop
			 * repeats.
			 */

			if (tempMarks.get(j).equals(".")) {
				result = numbers.get(i) + "." + numbers.get(i + 1);
				tempMarks.remove(j);
				numbers.remove(i);
				numbers.remove(i);
				numbers.add(i, result);
				// j-- and i-- is added so that no marks would be missed as we
				// remove a mark.
				j--;
				i--;
			}
			// if it's not a mark then 1 is added to j so it w ouldn't stay
			// still.
			j++;
		}
		// All the strings are added to doubleArray as Double values.
		for (int i = 0; i < numbers.size(); i++) {
			doubleArray.add(Double.parseDouble(numbers.get(i)));
		}
		// New doubleArray is returned with all the double values created.
		return doubleArray;
	}
}