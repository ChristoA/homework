package Calculator11;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class GUI extends Application {

	@Override
	public void start(Stage myStage) {

		myStage.setTitle("Calculator");

		// Creating GridPane with alignments and gaps
		GridPane rootNode = new GridPane();
		rootNode.setHgap(3);
		rootNode.setVgap(3);
		rootNode.setAlignment(Pos.CENTER);

		// Creating scene,
		Scene myScene = new Scene(rootNode, 250, 145);
		myStage.setResizable(false);

		// Adding equation TextField
		rootNode.add(new Label("The equation:"), 0, 0);
		TextField equation = new TextField();
		equation.setPrefWidth(175);
		rootNode.add(equation, 0, 1);

		// Adding button to calculate
		Button calculate = new Button("Calculate");
		rootNode.add(calculate, 0, 2);
		GridPane.setHalignment(calculate, HPos.RIGHT);

		// Adding result TextField
		rootNode.add(new Label("The result:"), 0, 2);
		TextField result = new TextField();
		result.setEditable(false);
		rootNode.add(result, 0, 3);
		
		// Adding button to expand the GUI
		Button expand = new Button("->");
		rootNode.add(expand, 0, 4);
		GridPane.setHalignment(expand, HPos.RIGHT);
		expand.setOnAction(event -> {
			buttons(rootNode, equation, expand, myScene);
			rootNode.getChildren().remove(expand);
		});
		
		// Calculate button starts the program
		calculate.setOnAction(event -> {
			result.setText(Result.main(equation.getText()));
		});

		// Pressing enter starts the program
		equation.setOnKeyReleased(event -> {
			if (event.getCode() == KeyCode.ENTER) {
				result.setText(Result.main(equation.getText()));
			}
		});

		myStage.setScene(myScene);

		myStage.show();
	}

	private void buttons(GridPane rootNode, TextField equation, Button expand, Scene myScene) {

		Button hide = new Button("<-");
		rootNode.add(hide, 0, 4);
		GridPane.setHalignment(hide, HPos.RIGHT);
		
		// Creating number buttons
		Button number1 = new Button("1");
		Button number2 = new Button("2");
		Button number3 = new Button("3");
		Button number4 = new Button("4");
		Button number5 = new Button("5");
		Button number6 = new Button("6");
		Button number7 = new Button("7");
		Button number8 = new Button("8");
		Button number9 = new Button("9");
		Button number0 = new Button("0");


		// Adding number buttons
		rootNode.add(number1, 1, 1);
		rootNode.add(number2, 2, 1);
		rootNode.add(number3, 3, 1);
		rootNode.add(number4, 1, 2);
		rootNode.add(number5, 2, 2);
		rootNode.add(number6, 3, 2);
		rootNode.add(number7, 1, 3);
		rootNode.add(number8, 2, 3);
		rootNode.add(number9, 3, 3);
		rootNode.add(number0, 2, 4);

		// Defining number button actions
		number1.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "1");
		});
		number2.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "2");
		});
		number3.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "3");
		});
		number4.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "4");
		});
		number5.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "5");
		});
		number6.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "6");
		});
		number7.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "7");
		});
		number8.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "8");
		});
		number9.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "9");
		});
		number0.setOnAction(event-> {
			equation.requestFocus();
			equation.setText(equation.getText() + "0");
		});
		
		// defining hide button action
		hide.setOnAction(event-> {
			equation.requestFocus();
			rootNode.getChildren().remove(number1);
			rootNode.getChildren().remove(number2);
			rootNode.getChildren().remove(number3);
			rootNode.getChildren().remove(number4);
			rootNode.getChildren().remove(number5);
			rootNode.getChildren().remove(number6);
			rootNode.getChildren().remove(number7);
			rootNode.getChildren().remove(number8);
			rootNode.getChildren().remove(number9);
			rootNode.getChildren().remove(number0);
			rootNode.getChildren().remove(hide);
			rootNode.add(expand, 0, 4);
		});

	}

	public static void main(String[] args) {
		launch(args);
	}

}