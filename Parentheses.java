package Calculator11;

import java.util.ArrayList;

public class Parentheses {

	public static void ParenthesesCalculator(ArrayList<Double> numbers, ArrayList marks) {

		// find out if input has parantheses

		ArrayList<Double> array = new ArrayList(numbers);

		int k = 0;
		int t = 0;

		// loops around to check for opening parenthesis.
		for (int i = 0; i < marks.size(); i++) {
			if (marks.get(i).equals("(")) {
				// loops around to check for closing parenthesis.
				for (int j = 0; j < marks.size(); j++) {
					if (marks.get(j).equals(")")) {
						// removes every number after the opening parenthesis
						while (j < array.size() - 1) {
							array.remove(j + 1);
						}
						// removes every mark after and including the opening
						// parenthesis
						while (j < marks.size()) {
							marks.remove(j);
						}
						// removes every number before the opening parenthesis
						while (i >= k) {
							array.remove(0);
							k++;
							}
						// removes every mark before the opening parenthesis
						while (i >= t) {
							marks.remove(0);
							t++;
						}
					}
				}

			}
		}
		Calculator.main(numbers, marks);
	}
}