package Calculator11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class FailureChecks {

	public static String main(ArrayList<String> numbers, ArrayList marks) {

		// intNumbers is created to make every number into Integer.
		ArrayList<Integer> intNumbers = new ArrayList<Integer>();

		for (int i = 0; i < numbers.size(); i++) {
			intNumbers.add(Integer.parseInt(numbers.get(i)));
		}

		return CheckingMarks(intNumbers, marks);

	}

	public static String CheckingMarks(ArrayList<Integer> numbers, ArrayList<String> marks) {

		/*
		 * openingParentheses & closingParentheses are created to later add a 1,
		 * if there's a parentheses in the equation.
		 */
		int openingParentheses = 0;
		int closingParentheses = 0;

		/*
		 * A loop to check if ArrayList marks contains any parentheses and if it
		 * does then adds 1 to the int value.
		 */
		for (int j = 0; j < marks.size(); j++) {
			if (marks.get(j).equals(")"))
				closingParentheses++;
			if (marks.get(j).equals("("))
				openingParentheses++;
		}
		/*
		 * Tests if every opening parentheses has a closing parentheses, if not
		 * then it throws exception.
		 */
		if (closingParentheses != openingParentheses) {
			return "You are missing parentheses!";
//			throw new IllegalArgumentException("You are missing parentheses!");
		}

		/*
		 * testMarks is created to remove all parentheses from the marks
		 * ArrayList, as if this is done with original ArrayList then it removes
		 * from original ArrayList also.
		 */
		ArrayList<String> testMarks = new ArrayList<String>(marks);	
		
		/*
		 * Removes all parentheses with a while loop.
		 */
		while (testMarks.contains("(") && testMarks.contains(")")) {
			testMarks.remove(")");
			testMarks.remove("(");
		}

		/*
		 * Because there's always one less mark then numbers then it checks if
		 * the amount of numbers is same as the amount of marks + 1, if not then
		 * something is missing
		 */
		if (testMarks.size() + 1 != numbers.size()) {
			return "Missing a mark or a number!";
//			throw new IllegalArgumentException("You are missing a mark or a number!");
		}
		
		return ""; 
	}

}
