package Calculator11;

import java.util.ArrayList;

public class Calculator {

	public static Double main(ArrayList<Double> numbers, ArrayList marks) {

		/*
		 * double number is representative of the result value, this will be
		 * added to the ArrayList numbers.
		 */
		double number;

		/*
		 * int i is created here so it would only be declared once.
		 */
		int i;

		/*
		 * This is the first loop which checks iv the mark at "i" is a
		 * multiplication mark or a division mark.
		 */
		for (i = 0; i < marks.size(); i++) {
			/*
			 * If it's a multiplication mark then it sends numbers to the
			 * multiplication method and the value of i so it would know which
			 * numbers need to be evaluated. After this it removes both numbers
			 * from the equation and adds the resulting value. Also removes the
			 * mark from the marks. i-- because a mark was removed. (same
			 * applies to division)
			 */
			if (marks.get(i).equals("*")) {
				number = multiplication(numbers, i);
				numbers.remove(i);
				numbers.remove(i);
				numbers.add(i, number);
				marks.remove(i);
				i--;
			} else if (marks.get(i).equals("/")) {
				number = division(numbers, i);
				numbers.remove(i);
				numbers.remove(i);
				numbers.add(i, number);
				marks.remove(i);
				i--;
			}

		}

		/*
		 * As this is addition or subtraction result is taken from the first
		 * number and values are being added or removed from that value.
		 */
		double result = numbers.get(0);
		for (i = 0; i < marks.size(); i++) {

			for (i = 0; i < marks.size(); i++) {
				if (marks.get(i).equals("+")) {
					result = addition(numbers, i, result);
				} else if (marks.get(i).equals("-")) {
					result = subtraction(numbers, i, result);
				}
			}
		}

		// Returns the result from the class.
		return result;
	}

	private static double multiplication(ArrayList<Double> numbers, int place) {
		// number1 is the first value that will be multiplied.
		double number1 = numbers.get(place);
		// number2 is the second value that will be multiplied.
		double number2 = numbers.get(place + 1);

		// output is the result of the calculation
		double output = number1 * number2;
		return output;
	}

	private static double division(ArrayList<Double> numbers, int place) {
		// number1 is the first value that will be divided.
		double number1 = numbers.get(place);
		// number2 is the second value that will used to divide first number
		// with.
		double number2 = numbers.get(place + 1);

		// output is the result of the calculation
		double output = number1 / number2;
		return output;
	}

	private static double addition(ArrayList<Double> numbers, int place, double number) {
		// number 1 is the number after the first number
		double number1 = numbers.get(place + 1);
		// output is the value after the addition
		double output = number + number1;

		return output;

	}

	private static double subtraction(ArrayList<Double> numbers, int place, double number) {
		// number 1 is the number after the first number
		double number1 = numbers.get(place + 1);
		// output is the value after the addition
		double output = number - number1;

		return output;
	}

	public static Double ParenthesesCalculator(ArrayList<Double> numbers, ArrayList marks) {

		// find out if input has parentheses

		/*
		 * iNumbers & iMarks stand for insideNumbers and insideMarks which will
		 * only be modified here and will stay here
		 */
		ArrayList<Double> iNumbers = new ArrayList(numbers);
		ArrayList iMarks = new ArrayList(marks);

		/*
		 * Getting the indexes of closing and opening parenthesis.
		 */
		int openingParenthesis = marks.indexOf("(");
		int closingParenthesis = marks.indexOf(")");

		/*
		 * clears the parentheses from original marks and numbers ArrayList
		 */
		marks.subList(openingParenthesis, closingParenthesis + 1).clear();
		numbers.subList(openingParenthesis, closingParenthesis).clear();

		
		/*
		 * Clears everything before and after the opening and closing parentheses
		 * including the parentheses themselves. 
		 */
		iNumbers.subList(closingParenthesis, iNumbers.size()).clear();
		iNumbers.subList(0, openingParenthesis).clear();
		/*
		 * Clears everything before and after the opening and closing parentheses
		 * including the parentheses themselves. 
		 */
		iMarks.subList(closingParenthesis, iMarks.size()).clear();
		iMarks.subList(0, openingParenthesis + 1).clear();

		/*
		 * double input is the number after the calculations have been done inside the parentheses. 
		 * After that the result is added to the original numbers array. 
		 */
		double input = Calculator.main(iNumbers, iMarks);
		numbers.add(openingParenthesis, input);
		return Calculator.main(numbers, marks);

	}

}
