package Calculator11;

import java.util.ArrayList;
import java.util.List;

public class ReassembledCalculator {

	public static Double DividingUp(ArrayList function) {

		/*
		 * ArrayList Parentheses are created to hold all the parentheses found
		 * from reassembled arraylist
		 */
		ArrayList parentheses = new ArrayList();
		/*
		 * pIndex is created to hold the indexes of the parentheses to know
		 * which sublist to take after reordering.
		 */
		ArrayList<Integer> pIndex = new ArrayList();

		/*
		 * Checking if there's a parentheses and if there is then I add the
		 * index of the parentheses to pIndex and the parentheses itself to
		 * parentheses array.
		 */
		for (int i = 0; i < function.size(); i++) {
			if (function.get(i).equals("(")) {
				pIndex.add(i);
				parentheses.add("(");
			}

			if (function.get(i).equals(")")) {
				pIndex.add(i);
				parentheses.add(")");
			}
		}

		/*
		 * for loop to check if two parentheses besides each other are opening
		 * and closing ones and if they are then I add their index to the end of
		 * the pIndex. After doing this I clear the beginning of the pIndex and
		 * parentheses, otherwise this will go to infinity. Also because I
		 * remove 2 values I will minus i value by 1.
		 */
		for (int i = 0; i < parentheses.size(); i++) {
			if (parentheses.get(i).equals("(") && parentheses.get(i + 1).equals(")")) {
				pIndex.add(pIndex.get(i));
				pIndex.add(pIndex.get(i + 1));
				pIndex.subList(i, i + 2).clear();
				parentheses.subList(i, i + 2).clear();
				i = -1;
			}
		}

		/*
		 * List toSeperation is created from original function with indexes that
		 * we just gathered. So only the values between parentheses are sent.
		 */
		List toSeperation = function.subList(pIndex.get(0) + 1, pIndex.get(1));
		/*
		 * toSeperation (the final calculation), pIndex (indexes of the
		 * parentheses) and original function is sent to ToMarksNumbers.
		 */

		return ToMarksNumbers(toSeperation, pIndex, function);
	}

	public static double ToMarksNumbers(List function, ArrayList<Integer> pIndex, ArrayList originalFunction) {

		/*
		 * ArrayList marks & numbers are created to hold the newly separated
		 * marks and numbers.
		 */
		ArrayList<String> marks = new ArrayList<String>();
		ArrayList<Double> numbers = new ArrayList<Double>();

		/*
		 * A loop to check if the object at function spot i is a number or a
		 * mark Because double is always longer than 1 character, then it can
		 * never be 1 Using this logic I will separate marks from numbers (Marks
		 * are always 1 character long)
		 */
		for (int i = 0; i < function.size(); i++) {
			// if length value is bigger than 1 then it's a number and added to
			// numbers array
			if (function.get(i).toString().length() > 1) {
				numbers.add(Double.parseDouble(function.get(i).toString()));
			} else {
				/*
				 * if length value isn't bigger 1 then it's a mark and is added
				 * to marks array
				 */

				marks.add(function.get(i).toString());
			}
		}

		/*
		 * double result is created to hold the calculation result of the
		 * calculations inside the parentheses
		 */
		Double result = Calculator.main(numbers, marks);

		/*
		 * integer removal is made to remember pIndex value at 0. (otherwise it
		 * created problems)
		 */
		int removal = pIndex.get(0);

		/*
		 * This loop removes everything between pIndex value at 0 and pIndex
		 * value at 1 and to it is added number 1. So essentially it removes the
		 * parentheses calculation that was just calculated
		 */
		for (int i = pIndex.get(0); i < pIndex.get(1) + 1; i++) {
			originalFunction.remove(removal);
		}

		/*
		 * Adds the result to the originalFunction to the spot of removal value.
		 */
		originalFunction.add(removal, result);

		/*
		 * If the originalFunction still has opening and closing parentheses
		 * then it will be sent to do another round to remove them.
		 */
		if (originalFunction.contains("(") && originalFunction.contains(")")) {
			result = ReassembledCalculator.DividingUp(originalFunction);
		} else {
			/*
			 * Clears marks and numbers. 
			 */
			marks.clear();
			numbers.clear();
			for (int i = 0; i < originalFunction.size(); i++) {
				/*
				 * Separates the numbers and marks and then they are send to the
				 * calculator for the last evaluation.
				 */
				if (originalFunction.get(i).toString().length() > 1) {
					numbers.add(Double.parseDouble(originalFunction.get(i).toString()));
				} else {
					marks.add(originalFunction.get(i).toString());
				}
			}
			result = Calculator.main(numbers, marks);

		}
		return result;
	}

}
