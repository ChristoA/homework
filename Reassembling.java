package Calculator11;

import java.util.ArrayList;

public class Reassembling {

	public static ArrayList<String> reassemble(ArrayList<Double> numbers, ArrayList<String> marks) {

		// This is the array that everything will be reconstructed in.
		ArrayList reconstruction = new ArrayList();

		int j = 0;

		for (int i = 0; i < marks.size(); i++) {
			/*
			 * If i+1 is marks size then it adds the last mark. If i+1 is marks
			 * size and j+1 is numbers size then it adds the number to the end
			 * to avoid any missing parentheses or marks
			 */
			if (i + 1 == marks.size()) {
				if (j + 1 == numbers.size()) {
					reconstruction.add(numbers.get(j++));
				}
				reconstruction.add(marks.get(i));
				break;
			}

			/*
			 * I cannot be 0 because marks doesn't have a -1 place. If i is
			 * bigger than 1, previous mark is a closing parentheses and next
			 * mark is opening parentheses, then it adds current mark, this is
			 * there because there must be a mark between two parentheses.
			 */
			if (i > 1 && marks.get(i - 1).equals(")") && marks.get(i + 1).equals("(")) {
				reconstruction.add(marks.get(i));

				/*
				 * If current mark is closing parentheses and next one isnt,
				 * then it adds current mark, adds 1 to i and adds next mark
				 * also because on the both sides of the number must be a mark.
				 */
			} else if (marks.get(i).equals(")") && !marks.get(i + 1).equals(")")) {				
				reconstruction.add(marks.get(i++));
				reconstruction.add(marks.get(i));
				if (marks.size() > i+1 && marks.get(i-1).equals(")") && !marks.get(i).equals(")") && marks.get(i+1).equals(")")) {
					reconstruction.add(numbers.get(j++));
				}

				/*
				 * If next i is the marks size and next j isn't numbers size
				 * then it adds another number (This is so that if the equation
				 * ends with ... y-x)+b then the b will be added.
				 */
				if (i + 1 == marks.size() && j != numbers.size()) {
					reconstruction.add(numbers.get(j++));
				}

				/*
				 * If the current mark isn't a parentheses, then it adds the
				 * number, after that it adds a mark and if the next mark is
				 * closing parentheses then it also adds next number.
				 */
			} else if (!marks.get(i).equals("(") && !marks.get(i).equals(")")) {
				reconstruction.add(numbers.get(j++));
				reconstruction.add(marks.get(i));
				if (marks.get(i + 1).equals(")")) {
					reconstruction.add(numbers.get(j++));
				}
				/*
				 * If the current mark is parentheses then it adds it.
				 */
			} else if (marks.get(i).equals("(") || marks.get(i).equals(")")) {
				reconstruction.add(marks.get(i));
			}
		}
		// Returns reconstructed ArrayList
		return reconstruction;

	}

}
